create database gamedb if not exists;
use gamedb;



create table right_answers(
id int auto_increment,
right_answer varchar(512) not null,
right_answer_id varchar(512) not null,
primary key(id)
);

create table answers(
id int auto_increment,
answer1 varchar(512) not null,
answer2 varchar(512) not null,
answer3 varchar(512) not null,
answer4 varchar(512) not null,
primary key(id),
foreign key (id) references right_answers (id)
);


create table questions(
id int auto_increment,
question varchar(512) not null,
primary key(id),
foreign key (id) references answers (id)
);



INSERT INTO `right_answers` (`id`, `right_answer`, `right_answer_id`) VALUES
('1', 'meta-knowledge', '4'),
('2', 'objects', '3'),
('3', 'performance', '1'),
('4', 'events', '4'),
('5', 'representational adequacy', '2'),
('6', 'inferential adequacy', '4'),
('7', 'inferential efficiency', '1'),
('8', 'acquisitional efficiency', '4'),
('9', 'heuristic mechanism', '2'),
('10', 'ab', '1'),
('11', 'constraints', '2'),
('12', 'test', '3'),
('13', 'repairs', '4'),
('14', 'forward checking', '4'),
('15', 'partial look ahead', '1'),
('16', 'full look ahead', '1'),
('17', 'backward checking', '1'),
('18', '1958', '3'),
('19', '5', '2'),
('20', '4', '1'),
('21', 'reproduction', '4'),
('22', 'premature convergence', '1'),
('23', '2', '2'),
('24', 'dancing', '3'),
('25', 'Friedberg', '1'),
('26', 'creating the poulation of the possible solutions', '2'),
('27', 'data', '3'),
('28', 'learning from mistakes', '4'),
('29', 'inductive', '2'),
('30', 'deductive', '1'),
('31', 'automatic generation of composite programmes', '1'),
('32', '3', '2'),
('33', 'deductive-inductive-abductive', '1'),
('34', 'valid arguments', '4'),
('35', 'simpliest basic form', '3'),
('36', 'relations', '1'),
('37', 'Minsky', '2'),
('38', 'Schank', '4'),
('39', 'classes-objects-concepts-values', '3'),
('40', '2', '2'),
('41', 'propositional-predicate', '3'),
('42', 'extended propositional logic', '1'),
('43', 'terms-predicates-quantifiers', '4'),
('44', 'is_a', '2'),
('45', 'a_kind_of', '3'),
('46', '2', '4'),
('47', 'deduction-production', '3'),
('48', 'all the above', '4'),
('49', '3', '3'),
('50', '6', '3'),
('51', 'random rule', '1'),
('52', 'priority rule', '2'),
('53', 'rule activated by recent data', '3'),
('54', 'specific rule', '4'),
('55', 'rule that has benn chosen for the same data', '4'),
('56', 'none of the above', '4'),
('57', 'data and partial conclusions', '1'),
('58', 'a modal logic form', '3'),
('59', 'computational tree logic', '2'),
('60', 'represation of the temporal evaluation of the situation of a world', '3'),
('61', '5', '3'),
('62', 'time', '3'),
('63', 'next month', '4'),
('64', 'finishes', '4'),
('65', 'unequal', '3'),
('66', 'Turing', '4'),
('67', 'Fortran', '3'),
('68', 'the prehistoric period', '3'),
('69', 'the post modern period', '3'),
('70', 'simplicity', '2'),
('71', 'none of the above', '4'),
('72', 'an efficient and complete algorithm', '1'),
('73', 'an efficient but non complete algorithm', '2'),
('74', 'a modification of the Hill-Climbing Search', '3'),
('75', 'a second modification of Hill-Climbing Search', '1'),
('76', 'an upgraded HC ans BFS algorithm', '3'),
('77', 'a non efficient but complete algorithm', '3'),
('78', 'like Best FS and belong to the same categorie', '1'),
('79', 'similar to Minimax algorithm', '1'),
('80', 'the generate and test method', '4'),
('81', 'the AC-3', '3'),
('82', 'the last modification of HC', '4'),
('83', 'a combination of DFS and BFS search algorithms', '3'),
('84', '60-64', '2'),
('85', 'AI is different to the HI', '3'),
('86', 'to caracterize a machine as an intelligent one', '2'),
('87', 'a machine', '4'),
('88', 'a branch of algebra', '3'),
('89', '3', '2'),
('90', 'EDVAC', '4'),
('91', 'conjuction-disjunction-negation', '4'),
('92', 'ENIAC', '3'),
('93', 'Electronic Numerical Integrator and Calculator', '2'),
('94', 'Claud Shannon', '3'),
('95', 'won by computer', '3'),
('96', 'won by computer', '1'),
('97', 'Darwins Theory of evolution', '4'),
('98', 'Stanford Research Institute', '2'),
('99', 'John McCarthy', '1'),
('100', 'John McCarthy', '4');





INSERT INTO `answers` (`id`, `answer1`, `answer2`, `answer3`, `answer4`) VALUES
('1', 'objects', 'events', 'performance', 'meta-knowledge'),
('2', 'meta-knowledge', 'performance', 'objects', 'events'),
('3', 'performance', 'events', 'meta-knowledge', 'objects'),
('4', 'meta-knowledge', 'objects', 'performance', 'events'),
('5', 'acquisitional efficiency', 'representational adequacy', 'inferential efficiency', 'inferential adequacy'),
('6', 'inferential efficiency', 'acquisitional efficiency', 'representational adequacy', 'inferential adequacy'),
('7', 'inferential adequacy', 'representational adequacy', 'acquisitional efficiency', 'inferential efficiency'),
('8', 'representational adequacy', 'inferential adequacy', 'inferential efficiency', 'acquisitional efficiency'),
('9', 'greedy mechanism', 'heuristic mechanism', 'internal mechanism', 'external mechanism'),
('10', 'ab', 'minimax', 'delta', 'multimax'),
('11', 'contstraint domain', 'constraints', 'scheduling', 'planning'),
('12', 'creation', 'consumption', 'test', 'experiments'),
('13', 'structural', 'ethics', 'muteins', 'repairs'),
('14', 'partial look ahead', 'full look ahead', 'backward checking', 'forward checking'),
('15', 'full look ahead', 'backward checking', 'forward checking', 'partial look ahead'),
('16', 'backward checking', 'forward checking', 'partial look ahead', 'full look ahead'),
('17', 'forward checking', 'backward checking', 'full look ahead', 'partial look ahead'),
('18', '1957', '1956', '1958', '1959'),
('19', '4', '5', '6', '7'),
('20', '4', '5', '6', '7'),
('21', 'restoration', 'selection', 'separation', 'reproduction'),
('22', 'premature convergence', 'mature convergence', 'fast convergence', 'new convergence'),
('23', '5', '2', '6', '3'),
('24', 'game playing', 'scheduling', 'dancing', 'routing'),
('25', 'Friedberg', 'Holland', 'Koza', 'Russel'),
('26', 'mating and giving birth to the offsprings', 'creating the poulation of the possible solutions', 'giving a score to each solution', 'creating a new population'),
('27', 'wisdom', 'specific knowledge', 'data', 'information'),
('28', 'making new mistake', 'making no more mistakes', 'saying the truth', 'learning from mistakes'),
('29', 'deductive', 'inductive', 'abductive', 'constructive'),
('30', 'deductive', 'inductive', 'abductive', 'constructive'),
('31', 'automatic generation of composite programmes', 'evaluation of composite programmes', 'optimization of composite programmes', 'giving birth to offsprings'),
('32', '2', '3', '4', '6'),
('33', 'deductive-inductive-abductive', 'deductive-moluctive-constructive', 'deductive-abductive-constractive', 'deductive-constructive-inductive'),
('34', 'statements', 'premises', 'arguments', 'valid arguments'),
('35', 'easiest form', 'unknown form', 'simpliest basic form', 'cheepest form'),
('36', 'relations', 'classes', 'objects', 'values'),
('37', 'Freindberg', 'Minsky', 'Brachman', 'Schank'),
('38', 'Freindberg', 'Minsky', 'Brachman', 'Schank'),
('39', 'classes-objects-concepts-families', 'classes-values-concepts-families', 'classes-objects-concepts-values', 'classes-concepts-values-families'),
('40', '1', '2', '3', '4'),
('41', 'propositional-additional', 'predicate-additional', 'propositional-predicate', 'additional-subtracting'),
('42', 'extended propositional logic', 'evaluated propositional logic', 'estimated propositional logic', 'reformed propositional logic'),
('43', 'terms-facts-statements', 'terms-predicates-statements', 'terms-facts-predicates', 'terms-predicates-quantifiers'),
('44', 'is_apart', 'is_a', 'is_another', 'is_among'),
('45', 'a_kind_over', 'a_kind_on', 'a_kind_of', 'a_kind_off'),
('46', '4', '5', '3', '2'),
('47', 'production-substraction', 'deduction-substraction', 'deduction-production', 'substraction-additional'),
('48', 'rule base', 'working memory', 'control and conflict resolution', 'all the above'),
('49', '1', '2', '3', '4'),
('50', '2', '4', '6', '8'),
('51', 'random rule', 'priority rule', 'rule activated by recent data', 'specific rule'),
('52', 'random rule', 'priority rule', 'rule activated by recent data', 'specific rule'),
('53', 'random rule', 'priority rule', 'rule activated by recent data', 'specific rule'),
('54', 'random rule', 'priority rule', 'rule activated by recent data', 'specific rule'),
('55', 'random rule', 'specific rule', 'priority rule', 'rule that has benn chosen for the same data'),
('56', 'random rule', 'specific rule', 'priority rule', 'none of the above'),
('57', 'data and partial conclusions', 'data and metadata', 'metadata and partial conclusions', 'data and conflict resolution'),
('58', 'a constant logic form', 'a new logic form', 'a modal logic form', 'none of the above'),
('59', 'computational transport level', 'computational tree logic', 'computational town logic', 'computational town level'),
('60', 'represation of the structure of a frame', 'represation of a logic structure', 'represation of the temporal evaluation of the situation of a world', 'none of the above'),
('61', '1', '3', '5', '7'),
('62', 'globally', 'future', 'time', 'next time'),
('63', 'release', 'until', 'next time', 'next month'),
('64', 'overlaps', 'precedes', 'meets', 'finishes'),
('65', 'equal', 'during', 'unequal', 'ends'),
('66', 'McCarthy', 'Minsky', 'Shannon', 'Turing'),
('67', 'Lisp', 'Basic', 'Fortran', 'Prolog'),
('68', 'the classic period', 'the romantic period', 'the prehistoric period', 'the modern period'),
('69', 'the modern period', 'the new era period', 'the post modern period', 'the expert period systems'),
('70', 'complexity', 'simplicity', 'efficiency', 'completeness'),
('71', 'a fixed number', 'a number between 2 and 5', 'a factor of creations', 'none of the above'),
('72', 'an efficient and complete algorithm', 'an efficient algorithm', 'a complete algorithm', 'none of the above'),
('73', 'an efficient and complete algorithm', 'an efficient but non complete algorithm', 'a non efficient but complete algorithm', 'a non efficient and non complete algorithm'),
('74', 'an upgraded Hill-Climbing Search', 'a modern Hill-Climbing Search', 'a modification of the Hill-Climbing Search', 'a modification of the Bi-directional Search'),
('75', 'a second modification of Hill-Climbing Search', 'a modification of IDS', 'an upgraded Hill-Climbing Search', 'a second edition of Hill-Climbing Search'),
('76', 'an upgraded HC algorithm', 'an upgraded BFS algorithm', 'an upgraded HC ans BFS algorithm', 'an upgraded BFS and SA algorithm'),
('77', 'an efficient and complete algorithm', 'an efficient but non complete algorithm', 'a non efficient but complete algorithm', 'a non efficient and non complete algorithm'),
('78', 'like Best FS and belong to the same categorie', 'like HC algorithm', 'like BS algorithm', 'an upgraded Best FS algorithm'),
('79', 'similar to Minimax algorithm', 'similar to the A-star algorithm', 'an upgraded A-star algorithm', 'a new modification of the A-star algorithm'),
('80', 'the upgrade and test method', 'the compare and test method', 'the reproduce and test method', 'the generate and test method'),
('81', 'the AC-1', 'the AC-2', 'the AC-3', 'the AC-4'),
('82', 'an upgraded Bean search', 'a modification of beam search', 'a modification of BiS', 'the last modification of HC'),
('83', 'an updated Depth-First search', 'an updated Breath-First search', 'a combination of DFS and BFS search algorithms', 'none of the above'),
('84', '55-59', '60-64', '65-69', '70-74'),
('85', 'AI is better than HI', 'AI is equal to the HI', 'AI is different to the HI', 'AI and HI are the same'),
('86', 'measure the intelligence of a machine', 'to caracterize a machine as an intelligent one', 'to compare a machine to a man', 'estimate the completeness of a machine'),
('87', 'a mechanism', 'an effort', 'a phenomenon', 'a machine'),
('88', 'a new mechanism', 'a function', 'a branch of algebra', 'digital electronics'),
('89', '2', '3', '4', '1'),
('90', 'INVAC', 'ENMAC', 'ERVAC', 'EDVAC'),
('91', 'conjunction-addition-multiplication', 'addition-disjunction-multiplication', 'addition-conjunction-disjunction', 'conjuction-disjunction-negation'),
('92', 'INVAC', 'ENMAC', 'ENIAC', 'EDVAC'),
('93', 'Electrical Numerical Integrator and Computing', 'Electronic Numerical Integrator and Calculator', 'Electrical Numerical Integration and computing', 'Electronic Numerical Integrator and Calculus'),
('94', 'John McCarthy', 'Martin Minsky', 'Claud Shannon', 'Alen Turing'),
('95', 'no winner', 'won by Kasparov', 'won by computer', 'the match was interrupted'),
('96', 'won by computer', 'won by Kasparov', 'no winner', 'the match was interrupted'),
('97', 'Boolean algebra', 'DNA', 'human reactions', 'Darwins Theory of evolution'),
('98', 'MIT', 'Stanford Research Institute', 'Texas University', 'IBM'),
('99', 'John McCarthy', 'Marvin Minsky', 'Claud Shannon', 'Alan Turing'),
('100', 'Marvin Minsky', 'Alan Turing', 'Arthur Samuel', 'John McCarthy');



INSERT INTO `questions` (`id`, `question`) VALUES
('1', 'What kind of knowledge representation methods feature is the representation of knowledge of knowledge, i.e. knowledge of what is known to someone?'),
('2', 'What kind of knowledge representation methods feature is the representation of material goods of the world?'),
('3', 'What kind of knowledge representation methods feature is the representation of skills in how someone does things?'),
('4', 'What kind of knowledge representation methods feature is the representation of actions that occur in a world and their time sequence?'),
('5', 'Which criterias feature for evaluating knowledge representation methods is working with mechanisms that process existing knowledge structures ?'),
('6', 'Which criterias feature for evaluating knowledge representation methods is knowledge about changes in the world?'),
('7', 'Which criterias feature for evaluating knowledge representation methods is the ability to be able to introduce additional information into knowledge structures?'),
('8', 'Which criterias feature for evaluating knowledge representation methods is the ability to allow new knowledge to be acquired quickly and easily?'),
('9', 'What is the knowledge-based strategy called for the particular problem, which is used as an immediate solution with?'),
('10', 'Which algorithm belongs to the heuristic algorithm class;'),
('11', 'Limitation problems are typically represented by a set of variables and'),
('12', 'The simplest method to solve constraint satisfaction problems is the production and the ..'),
('13', 'Algorithms that try to improve a proposed solution gradually until they reach the desired result are called'),
('14', 'Which control clears values ​​from the domains of unbound variables directly associated with constraints with the variable to which only a value was assigned?'),
('15', 'Which control looks at all non-blocked variables fields in a pre-set order by checking their limitations once?'),
('16', 'Which control applies a complete consistency algorithm to every step of the search?'),
('17', 'Which of the following does not belong to the 3 basic ways of combining search algorithm and price filtering method?'),
('18', 'The introduction of genetic algorithms was made by Friedberg the ..'),
('19', 'How many components does a genetic algorithm contain?'),
('20', 'How many different nucleotides are the component elements of the dna?'),
('21', 'What is the process of producing offspring?'),
('22', 'Which of the following problems often appear in genetic algorithms?'),
('23', 'How many structural elements does the genetic algorithm method have?'),
('24', 'Genetic algorithms are successfully used in order to solve problems in different sectors.One of the following answers is wrong.Which one?'),
('25', 'In 1975, series of bits were introduced to unprove genetic programming by..'),
('26', 'The first step of the philosophy of genetic algorithms is..'),
('27', 'Knowledge is considered the result of four (4) different steps, which is the first one?'),
('28', 'Wisdom can be based also on..'),
('29', 'The most useful reasoning for a better computer aided knowledge is..'),
('30', 'The reasoning that garantees correct conclusions is ..'),
('31', 'Genetic programming is a process of ..'),
('32', 'How many are the basic reasoning methods ?'),
('33', 'The basic reasoning methods are ..'),
('34', 'Mathematical logic is the professional study of..'),
('35', 'Propositional logic is the ... of mathematical logic'),
('36', 'The sematic net consists of nodes and links. The links refer to ..'),
('37', 'The frames of shemata were defined by ..'),
('38', 'The script consists of a series of facts in a specific context. The scripts were initially proposed by ..'),
('39', 'The nodes of semantic net suggests:'),
('40', 'How many forms of mathematical logic are there?'),
('41', 'The forms of mathematical logic are..'),
('42', 'The predicate logic is an ..'),
('43', 'The predicate logic extents the propositional logic by introducing..'),
('44', 'A semantic net way include a relation ISA. ISA means ..'),
('45', 'A semantic net way include a relation AKO. AKO means ..'),
('46', 'In how many categories can the rule systems be devided ,according to the kind of rules that are used?'),
('47', 'The big categories of rule systems are..'),
('48', 'A production system includes the following sectors:'),
('49', 'The number of different sectors of a production system is ..'),
('50', 'The strategies designed for a conflict resolution are..'),
('51', 'The random strategy consists of choosing a ...'),
('52', 'The ordering strategy consists of choosing a ...'),
('53', 'The recent strategy consists of choosing a ...'),
('54', 'The specificity strategy consists of choosing a ...'),
('55', 'The refractoriness strategy consists of choosing a ...'),
('56', 'The means-ends strategy consists of choosing a ...'),
('57', 'The working memory elements are:'),
('58', 'What is temporal logic?'),
('59', 'What CTL means?'),
('60', 'What is Kripke structure?'),
('61', 'How many basic temporal operators are included in a CTL?'),
('62', 'Three of the following answers are basic temporal operators of a Computational Tree Logic. Which is the wrong one?'),
('63', 'Three of the following answers are basic temporal operators of a Computational Tree Logic. Which is the wrong one?'),
('64', 'The time internal logic examinates the relations that may exist between different facts according to time. There are seven basic binary relations. One of the following answers is wrong. Which one?'),
('65', 'There are seven binary relations between different facts, according to time. One of the following answers is wrong. Which one?'),
('66', 'The father of the Artificial Intelligence is ..'),
('67', 'The first programming language is ...'),
('68', 'The first period of Artificial Intelligence is called:'),
('69', 'The fifth period <today> of Artificial Intelligence is called:'),
('70', 'Which of the following answers, concerning the characteristics of the search algorithm, is wrong?'),
('71', 'The branching factor is ..'),
('72', 'The ID search algorithm is ..'),
('73', 'The Hill-Climbing Search is ..'),
('74', 'The Enforced Hill Climbing is ..'),
('75', 'The Simulated Annealing is:'),
('76', 'The Beam Search is..'),
('77', 'The Best-First Search is ..'),
('78', 'The A-star algorithm is ..'),
('79', 'The Alpha-Beta algorithm is ..'),
('80', 'The simpliest method of solving a constraint satisfaction problem is..'),
('81', 'The simpliest algorithm of Arc Consistency is..'),
('82', 'The Tabu Search is:'),
('83', 'The Iteractive Deepening search algorithm is:'),
('84', 'How old is Artificial Intelligence today, on 2017?'),
('85', 'If we compare Artificial Intelligence to the Human Intelligence we find that:'),
('86', 'The Turing test is a test in order to:'),
('87', 'The surname of Alan Turing was given to a test but also to:'),
('88', 'The surname of George Boole was given by Sheffer on 1913 to:'),
('89', 'The main operations of Boolean Algebra are:'),
('90', 'John Von Newman during 1945 designed the first calculator known also as Von Newman machine. Its name was:'),
('91', 'The main operations of Boolean Algebra are:'),
('92', 'In 1946 the first electronic calculator was manufactured in Pensylvania. Its name was:'),
('93', 'ENIAC means:'),
('94', 'In 1948 the Mathematical Theory of Communication was published by:'),
('95', 'In 1997 a chess match took place between computer and Garry Kasparov. The result was:'),
('96', 'A few years after 1997 a second chess game between a computer and Vladimir Kramnik took place. The result was:'),
('97', 'The main mechanism of genetic algorithms is based on:'),
('98', 'The first robot named shakey was created at ...'),
('99', 'In 1956 during a meeting at Dartmouth of Massachusetts the name of Artificial Intelligence was proposed by:'),
('100', 'In 1958 the functional programming language LISP was developed  by:');
